---
title: "Home"
date: 2018-03-24T19:55:15-03:00
draft: false
---

LKCAMP is a Linux Kernel study group open for everyone to participate.

Please, see [About](/about) and [Documentation]
(https://lkcamp.gitlab.io/lkcamp_docs/) pages for more information and how to
get started.

## Round 3 - Campinas/Brazil

![lkcamp-round3-flyer](imgs/lkcamp-divulgacao.png)


### Attending the meetings
- **When:** Every Tuesday at 19h30 (Sao Paulo time) starting on 26 Mar 2019
- **Where:** [room 322 IC3 @ Unicamp (Campinas/SP Brazil)](https://www.openstreetmap.org/way/95011154)
- **Subscription**: Please fill the form [http://lkcamp.persona.ninja/limesurvey/index.php/875677?lang=pt-BR](http://lkcamp.persona.ninja/limesurvey/index.php/875677?lang=pt-BR)

### Remote participation
- **Live stream:** [YouTube IC channel]
(https://www.youtube.com/channel/UCraCE6iWUcFCJSp-vmO1D3A/videos) 
- **Mailing list:** [lkcamp@lists.libreplanetbr.org]
(https://lists.libreplanetbr.org/mailman/listinfo/lkcamp)
- **IRC Channel:** #lkcamp @ Freenode
- **Telegram (IRC bridge):** [https://t.me/lkcamp](https://t.me/lkcamp)

# Mettings

- [M1: Welcome to LKCAMP!](https://lkcamp.gitlab.io/lkcamp_docs/unicamp_group/boot/)
    - When: 26/Mar/2019
    - Presenter: Helen Koike
    - [slides](/slides/round3/lkcamp-M1.pdf)
    - [video](https://www.youtube.com/watch?v=BmBzNDzLi0w)

- [M2: First Contribution](https://lkcamp.gitlab.io/lkcamp_docs/unicamp_group/first_contrib/)
    - When: 02/Apr/2019
    - Presenters: Gabriel Mandaji / Danilo Rocha
    - [slides](/slides/round3/lkcamp-M2.pdf)
    - [video](https://www.youtube.com/watch?v=VCwFkbxoY7I)

- [M3: Device Drivers](https://lkcamp.gitlab.io/lkcamp_docs/unicamp_group/dev_drivers/)
    - When: 09/Apr/2019
    - Presenters: Laís Pessine / Helen Koike
    - [slides](/slides/round3/lkcamp-M3.pdf)
    - [video](https://www.youtube.com/watch?v=-ge-X0aUhJo)

- M4: How the system boots
    - When: 16/Apr/2019
    - Presenter: Lucas Magalhães
    - [slides](/slides/round3/lkcamp-M4.pdf)
    - [video](https://www.youtube.com/watch?v=XQE_pizWV8c)

- [M5: System calls](https://lkcamp.gitlab.io/lkcamp_docs/unicamp_group/systemcalls/)
    - When: 23/Apr/2019
    - Presenter: Nícolas F. R. A. Prado
    - [slides](/slides/round3/lkcamp-M5.pdf)
    - [video](https://www.youtube.com/watch?v=NCZiXmjYZHg)

## Previous rounds

Check our videos from previous rounds at [the archived](/archived/) page

## Round 1 - Blumenau-SC/Brazil

# Mettings

- M1: Introdução ao Kernel do Linux
    - When: 21/May/2019
    - Presenters: David Emmerich Jourdain / Marcos Paulo de Souza
    - [slides](/slides/round1-bnu/lkcamp-M1.pdf)
    - [video](https://www.youtube.com/watch?v=Sjx2QO_t0EM)

- M2: Hello World! Creating your first device driver in Linux Kernel
    - When: 25/Jun/2019
    - Presenters: Marcos Paulo de Souza

# Please edit our page

We are all volunteers, any help is welcomed.
The code of this page is available at [https://gitlab.com/lkcamp](https://gitlab.com/lkcamp)
If you want to help us to improve this page, please send us Pull Requests.
